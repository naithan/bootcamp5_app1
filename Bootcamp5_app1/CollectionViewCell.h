//
//  CollectionViewCell.h
//  Bootcamp5_app1
//
//  Created by Jeremi Kaczmarczyk on 17.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Pogoda.h"

@interface CollectionViewCell : UICollectionViewCell
//@property (strong, nonatomic) IBOutlet UIImageView *imageView;
//@property (nonatomic) UIImage *image;
@property (strong, nonatomic) IBOutlet UILabel *cityLabel;
@property (strong, nonatomic) IBOutlet UILabel *temperatureLabel;
@property Pogoda *currentWeather;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
