//
//  ViewController.m
//  Bootcamp5_app1
//
//  Created by Jeremi Kaczmarczyk on 17.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import "ViewController.h"
#import "CollectionViewCell.h"
#import <UIImageView+AFNetworking.h>
#import "Pogoda.h"

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) IBOutlet UICollectionView *imagesCollectionView;
@property (strong, nonatomic) IBOutlet UIButton *touchButton;
@property (weak, nonatomic) NSData *data;
@property (strong, nonatomic) NSMutableArray *capitalCities;
//@property Pogoda *aktualnaPogoda;
- (IBAction)touchButton:(id)sender;
@property int count;



@end

@implementation ViewController

static NSString * const reuseIdentifier = @"imageCellId";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setUp];
//    self.aktualnaPogoda = [Pogoda new];
//    self.imagesCollectionView.delegate = self;
//    self.imagesCollectionView.dataSource = self;
}

- (void)setUp {
    [self.imagesCollectionView reloadData];
    self.capitalCities = [[NSMutableArray alloc] init];
    [self setCapitalCities];
    [self.imagesCollectionView setBackgroundColor:[UIColor whiteColor]];
    self.count = 5;
    [self.touchButton.layer setCornerRadius:10];
}

- (IBAction)touchButton:(id)sender{
    [UIView animateWithDuration:0.2 animations:^{
        [self.touchButton setAlpha:0.2];
    } completion:^(BOOL finished){
        [UIView animateWithDuration:0.6 animations:^{
        [self.touchButton setAlpha:1];
        }];
    }];
    
    self.count +=5;
    [self.imagesCollectionView reloadData];
}


- (void)setCapitalCities{
    [self.capitalCities addObject:@"Tirana"];
    [self.capitalCities addObject:@"Andorra_la_Vella"];
    [self.capitalCities addObject:@"Yerevan"];
    [self.capitalCities addObject:@"Vienna"];
    [self.capitalCities addObject:@"Baku"];
    [self.capitalCities addObject:@"Minsk"];
    [self.capitalCities addObject:@"Brussels"];
    [self.capitalCities addObject:@"Sarajevo"];
    [self.capitalCities addObject:@"Sofia"];
    [self.capitalCities addObject:@"Zagreb"];
    [self.capitalCities addObject:@"Nicosia"];
    [self.capitalCities addObject:@"Prague"];
    [self.capitalCities addObject:@"Copenhagen"];
    [self.capitalCities addObject:@"Tallinn"];
    [self.capitalCities addObject:@"Helsinki"];
    [self.capitalCities addObject:@"Paris"];
    [self.capitalCities addObject:@"Tbilisi"];
    [self.capitalCities addObject:@"Berlin"];
    [self.capitalCities addObject:@"Athens"];
    [self.capitalCities addObject:@"Budapest"];
    [self.capitalCities addObject:@"Reykjavík"];
    [self.capitalCities addObject:@"Dublin"];
    [self.capitalCities addObject:@"Rome"];
    [self.capitalCities addObject:@"Astana"];
    [self.capitalCities addObject:@"Pristina"];
    [self.capitalCities addObject:@"Riga"];
    [self.capitalCities addObject:@"Vaduz"];
    [self.capitalCities addObject:@"Vilnius"];
    [self.capitalCities addObject:@"Luxembourg"];
    [self.capitalCities addObject:@"Skopje"];
    [self.capitalCities addObject:@"Valletta"];
    [self.capitalCities addObject:@"Chişinău"];
    [self.capitalCities addObject:@"Monaco"];
    [self.capitalCities addObject:@"Podgorica"];
    [self.capitalCities addObject:@"Amsterdam"];
    [self.capitalCities addObject:@"Oslo"];
    [self.capitalCities addObject:@"Warsaw"];
    [self.capitalCities addObject:@"Lisbon"];
    [self.capitalCities addObject:@"Bucharest"];
    [self.capitalCities addObject:@"Moscow"];
    [self.capitalCities addObject:@"San_Marino"];
    [self.capitalCities addObject:@"Belgrade"];
    [self.capitalCities addObject:@"Bratislava"];
    [self.capitalCities addObject:@"Ljubljana"];
    [self.capitalCities addObject:@"Madrid"];
    [self.capitalCities addObject:@"Stockholm"];
    [self.capitalCities addObject:@"Bern"];
    [self.capitalCities addObject:@"Ankara"];
    [self.capitalCities addObject:@"Kiev"];
    [self.capitalCities addObject:@"London"];
    [self.capitalCities addObject:@"Vatican_City"];
}

//- (UIImage *)getImage {
//
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
//    
//    NSURL *URL = [NSURL URLWithString:@"http://lorempixel.com/300/300/"];
//    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
//    
//    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
//        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
//        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
//    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
//        NSLog(@"File downloaded to: %@", filePath);
//        self.data = [NSData dataWithContentsOfURL:filePath];
//    }];
//    [downloadTask resume];
//    UIImage *image = [[UIImage alloc] initWithData:self.data];
//    return image;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return self.count;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    NSString *cityText = [self.capitalCities objectAtIndex:indexPath.row];
//    NSString *url = [NSString stringWithFormat:@"https://www.googleapis.com/customsearch/v1?key= AIzaSyCTwLWL3tKDHEn6hb9qw10WJoRxwVqmQnk&cx=002775750480719106944:u9vur2am3sy&q=%@&searchType=image&fileType=jpg&imgSize=small&alt=json",cityText];
//    NSURL *url2 = [NSURL URLWithString:url];
//
    
    //TODO: Obrazki z custom search api
    //Cos sie skopalo obrazki sa niezbyt losowe
    NSString *url = @"http://lorempixel.com/300/300/";
    NSURL *url2 = [NSURL URLWithString:url];
    
    if (!cell.imageView.image ) {
        [cell.imageView setImageWithURL:url2];
    }
    [cell.imageView setAlpha:0.4];

    [cell.layer setCornerRadius:20];
    [cell.layer setBorderWidth:5];
    [cell setBackgroundColor:[UIColor whiteColor]];
    
    cell.currentWeather = [Pogoda new];
    [cell.cityLabel setText:cityText];

    [cell.currentWeather getCurrent:cityText withCompletionBlock:^{
        cell.temperatureLabel.text = [NSString stringWithFormat:@"%2.1f°C", [cell.currentWeather tempCurrent]];
        if ([cell.currentWeather tempCurrent]<10) {
            cell.layer.borderColor = ([UIColor blueColor].CGColor);
            //[cell setBackgroundColor:[UIColor blueColor]];
        }else if ([cell.currentWeather tempCurrent]<15) {
            cell.layer.borderColor = ([UIColor yellowColor].CGColor);
            //[cell setBackgroundColor:[UIColor yellowColor]];
        } else {
            cell.layer.borderColor = ([UIColor redColor].CGColor);
            //[cell setBackgroundColor:[UIColor redColor]];
        };
    }];

    
//    cell.temperatureLabel.text = [NSString stringWithFormat:@"%2.1f°C", [cell.currentWeather tempCurrent]];
    
    return cell;
}

#pragma mark  - UICollectionViewDelegateFlowLayout

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
//    CGSize size;
//    size = CGSizeMake(300, 300);
//    return size;
//}
//
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
//    return 5;
//}
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
//    return 5;
//}
//
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
//    return UIEdgeInsetsMake(5, 50, 5, 5);
//}


@end
