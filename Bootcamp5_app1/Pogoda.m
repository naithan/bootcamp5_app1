//
//  Pogoda.m
//  weatherApp3
//
//  Created by Jeremi Kaczmarczyk on 27.03.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import "Pogoda.h"

@implementation Pogoda {
    NSDictionary *weatherServiceResponse;
}

- (id)init {
    self = [super init];
    weatherServiceResponse = @{};
    return self;
}

- (void)getCurrent:(NSString *)query  withCompletionBlock:(void (^)(void))completion{
    NSString *const BASE_URL_STRING = @"http://api.openweathermap.org/data/2.5/weather";
    NSString *weatherURLText = [NSString stringWithFormat:@"%@?q=%@", BASE_URL_STRING, query];
    NSURL *weatherURL = [NSURL URLWithString:weatherURLText];
    NSURLRequest *weatherRequest = [NSURLRequest requestWithURL:weatherURL];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:weatherRequest];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id JSON){
        NSLog(@"Jest dobrze");
        NSLog(@"%@", weatherServiceResponse);
        weatherServiceResponse = (NSDictionary *)JSON;
        [self parseWeatherServiceResponse];
        completion();

    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        NSLog(@"Jest zle");
        weatherServiceResponse = @{};

    }];
    [operation start];
}

- (void)parseWeatherServiceResponse
{
    _cloudCover = [weatherServiceResponse[@"clouds"][@"all"] integerValue];
    
    _latitude = [weatherServiceResponse[@"coord"][@"lat"] doubleValue];
    _longitude = [weatherServiceResponse[@"coord"][@"lon"] doubleValue];
    
    _reportTime = [NSDate dateWithTimeIntervalSince1970:[weatherServiceResponse[@"dt"] doubleValue]];
    
    _humidity = [weatherServiceResponse[@"main"][@"humidity"] integerValue];
    _pressure = [weatherServiceResponse[@"main"][@"pressure"] integerValue];
    _tempCurrent =[Pogoda kelvinToCelcius:[weatherServiceResponse[@"main"][@"temp"] doubleValue]];
    _tempMax = [Pogoda kelvinToCelcius:[weatherServiceResponse[@"main"][@"temp_min"] doubleValue]];
    _tempMin = [Pogoda kelvinToCelcius:[weatherServiceResponse[@"main"][@"temp_max"] doubleValue]];
    
    _city = weatherServiceResponse[@"name"];
    
    _rainHours = [weatherServiceResponse[@"rain"][@"3h"] integerValue];
    _snowHours = [weatherServiceResponse[@"snow"][@"3h"] integerValue];
    
    _country = weatherServiceResponse[@"sys"][@"country"];
    _sunrise = [NSDate dateWithTimeIntervalSince1970:[weatherServiceResponse[@"sys"][@"sunrise"]
                                                      doubleValue]];
    _sunset = [NSDate dateWithTimeIntervalSince1970:[weatherServiceResponse[@"sys"][@"sunset"]
                                                     doubleValue]];
    _conditions = weatherServiceResponse[@"weather"];
    
    _windDirection = [weatherServiceResponse[@"wind"][@"dir"] integerValue];
    _windSpeed = [weatherServiceResponse[@"wind"][@"speed"] integerValue];
}

+ (double)kelvinToCelcius:(double)degreesKelvin{
    const double ZERO_CELCIUS_IN_KELVIN = 273.15;
    return degreesKelvin - ZERO_CELCIUS_IN_KELVIN;
}

@end
